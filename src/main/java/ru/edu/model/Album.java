package ru.edu.model;

import javax.xml.bind.annotation.XmlTransient;
import java.util.Optional;

public class Album {

    private String name;

    private long year;

    private Optional<CD> cd;

    public Album(String name, long year, CD cd) {
        this.name = name;
        this.year = year;
        this.cd = Optional.ofNullable(cd);
    }

    public Album(String name, long year) {
        this(name, year, null);
    }

    public Album() {
    }

    public String getName() {
        return name;
    }

    public long getYear() {
        return year;
    }

    @XmlTransient
    public String getArtistName() {
        return cd.orElse(new CD()).getName();
    }

    @XmlTransient
    public String getCountry() {
        return cd.orElse(new CD()).getCountry();
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
