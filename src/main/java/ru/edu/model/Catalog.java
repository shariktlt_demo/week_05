package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "CATALOG")
public class Catalog {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    List<CD> cdList = new ArrayList<>();


    public Catalog() {
    }

    public Catalog(List<CD> cdList) {
        this.cdList = cdList;
    }

    public List<CD> getCdList() {
        return cdList;
    }
}
