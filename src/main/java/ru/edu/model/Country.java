package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private String name;

    private List<Artist> artists = new ArrayList<>();


    public Country(String name) {
        this.name = name;
    }

    public Country() {
    }

    public String getName() {
        return name;
    }

    public List<Artist> getArtists() {
        return artists;
    }


    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", artists=" + artists +
                '}';
    }
}
