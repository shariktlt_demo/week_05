package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

public class Registry {

    private List<Country> countries = new ArrayList<>();



    public List<Country> getCountries() {
        return countries;
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public void addCountryAll(Registry registry){
        countries.addAll(registry.getCountries());
    }


    @Override
    public String toString() {
        return "Registry{" +
                "countries=" + countries +
                '}';
    }
}
