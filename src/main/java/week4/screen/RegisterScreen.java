package week4.screen;

import week4.Competition;
import week4.model.AtheteImpl;
import week4.model.Participant;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class RegisterScreen implements Screen {
    private int step = 0;
    private final String[] promptsForSteps = {
            "Введите имя: ", //step = 0
            "Введите фамилию: ", //step = 1
            "Введите страну: " //step = 2
    };

    private InputStream input;
    private PrintStream output;
    private Scanner scanner;

    private AtheteImpl.Builder athleteForRegistation = null;

    public RegisterScreen(Scanner input, PrintStream output) {
        //this.input = input;
        this.output = output;
        scanner = input;
    }

    RegisterScreen(int step, Scanner scanner, InputStream input, PrintStream output,
                   AtheteImpl.Builder athleteForRegistration) {
        this.step = step;
        this.scanner = scanner;
        this.input = input;
        this.output = output;
        this.athleteForRegistation = athleteForRegistration;
    }

    /**
     * Выводит текст на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void prompt() {

        if (step == 0) {
            athleteForRegistation = AtheteImpl.builder();
        }
        output.println(promptsForSteps[step]);

    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition is reqiered in input
     * @return the Screen type: 1). to go to next step;
     * 2). null: to delete this screen from stack
     */
    @Override
    public Screen readInput(Competition competition) {
//        Scanner scanner = new Scanner(input);
        String inputLine = scanner.nextLine(); // из сканера in.readline()
        if (step == 0) { // ввод имени
            athleteForRegistation.setFirstName(inputLine);
        }
        if (step == 1) { // ввод фамилия
            athleteForRegistation.setLastName(inputLine);
        }
        if (step == 2) {
            athleteForRegistation.setCountry(inputLine);

            try {
                AtheteImpl registerAthlete = athleteForRegistation.build();
                Participant registered = competition.register(registerAthlete);
                output.println("Участник Id#: " + registered.getId() +
                        ", ИМЯ: " + registered.getAthlete().getFirstName() +
                        ", ФАМИЛИЯ: " + registered.getAthlete().getLastName() +
                        " зарегистрирован");
            } catch (IllegalArgumentException | IllegalStateException e) {
                output.println("Ошибка: " + e.getMessage());
            }
            //output.println();
            return null; //возвращаемся на предыдущий экран
        }
        step++;
        return this;//остаемся на экране
    }
}
