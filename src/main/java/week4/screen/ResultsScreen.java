package week4.screen;

import week4.Competition;
import week4.model.Participant;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

public class ResultsScreen implements Screen {

    private InputStream input;

    private PrintStream output;

    public ResultsScreen(InputStream input, PrintStream output) {
        this.input = input;
        this.output = output;
    }


    @Override
    public void prompt() {
        output.println("Personal results are below:");
    }


    @Override
    public Screen readInput(Competition competition) {
        List<Participant> participantList = competition.getResults();
        for (Participant participant : participantList) {
            output.println("Id#: " + participant.getId() + " "
                    + "Name: " + participant.getAthlete().getFirstName() + " "
                    + "Surname: " + participant.getAthlete().getLastName() + " "
                    + "Country: " + participant.getAthlete().getCountry() + " "
                    + "Score: " + participant.getScore()
            );

        }
        output.println();
        return null;
    }

}
