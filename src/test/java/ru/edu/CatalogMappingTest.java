package ru.edu;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;
import ru.edu.model.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CatalogMappingTest {

    @Test
    public void test() throws Exception {
        ObjectMapper mapper = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Catalog catalog = mapper.readValue(new File("./input/cd_catalog.xml"), Catalog.class);

        Registry registry = catalog.getCdList().stream()
                .map(cd -> new Album(cd.getTitle(), cd.getYear(), cd))
                .collect(
                        HashMap::new,
                        this::insertAlbum,
                        HashMap::putAll
                )
                .values().stream()
                .collect(
                        HashMap::new,
                        this::insertCountry,
                        HashMap::putAll
                ).values().stream()
                .collect(
                        Registry::new,
                        Registry::addCountry,
                        Registry::addCountryAll
                );

        System.out.println(registry);
    }

    private void insertCountry(Map<String, Country> map, Artist artist) {
        String countryName = artist.getAlbums().get(0).getCountry();
        map.putIfAbsent(countryName, new Country(countryName));
        map.get(countryName).getArtists().add(artist);
    }


    private void insertAlbum(Map<String, Artist> map, Album album) {
        map.putIfAbsent(album.getArtistName(), new Artist(album.getArtistName()));
        map.get(album.getArtistName()).getAlbums().add(album);
    }
}
