package ru.edu;

import org.junit.Test;

import java.util.*;

public class IteratorExample {

    @Test
    public void iterator(){
        List<String> list = new ArrayList<>();

        list.add("one");
        list.add("two");
        list.add("three");


        for (String str : list){
            System.out.println(str);
        }

        Iterator<String> iterator = list.iterator();
        ListIterator<String> stringListIterator = list.listIterator();
        while (stringListIterator.hasNext()){
            String str = iterator.next();

            if("two".equals(str)){
                stringListIterator.previous();
            }

            iterator.remove();
        }



        Map<String, Object> map = new HashMap<>();

        for (String key : map.keySet()) {

        }

        for (Object value : map.values()) {

        }
        for (Map.Entry<String, Object> etnry: map.entrySet()){}
    }

    private static class Team implements Iterable<Team>{


        /**
         * Returns an iterator over elements of type {@code T}.
         *
         * @return an Iterator.
         */
        @Override
        public Iterator<Team> iterator() {
            return new Iterator<Team>() {
                int pos = 0;



                @Override
                public boolean hasNext() {
                    return false;
                }

                @Override
                public Team next() {
                    return null;
                }
            };
        }
    }
}
