package ru.edu;

import org.junit.Test;
import week4.model.Athlete;

import java.io.*;

public class SerializationExample {

    static class Example implements Serializable {
        String a;
        int b;
        boolean c;

        transient Athlete athlete;

        static long serialVersionUID = 1l;
    }

    @Test
    public void serialization() throws Exception {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("./file"));

        Example example = new Example();

        //лучше использовать готовое решение
        outputStream.writeObject(example);

        //либо свое
        outputStream.writeUTF(example.a);
        outputStream.writeInt(example.b);
        outputStream.writeBoolean(example.c);


        ObjectInputStream inputStream = null;

        //лучше использовать готовое решение
        Example o = (Example) inputStream.readObject();

        //либо свое
        Example readObject = new Example();
        readObject.a = inputStream.readUTF();
        readObject.b = inputStream.readInt();
        readObject.c = inputStream.readBoolean();
    }
}
