package ru.edu.model;

import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import static org.junit.Assert.*;

public class RegistryTest {

    @Test
    public void getCountries() throws Exception {

        Registry registry = new Registry();
        fillCountries(registry.getCountries());

        ObjectMapper mapper = new ObjectMapper();

        try(FileOutputStream fous = new FileOutputStream("./output/artist_by_country.json")){
            mapper.writeValue(fous, registry);
        }


    }

    private void fillCountries(List<Country> countries) {
        for (int i = 0; i < 3; i++) {
            Country country = new Country("country" + i);

            fillArtist(country.getArtists());
            countries.add(country);
        }
    }

    private void fillArtist(List<Artist> artists) {
        for (int i = 0; i < 3; i++) {
            artists.add(new Artist("Artist " + i));
        }
    }
}